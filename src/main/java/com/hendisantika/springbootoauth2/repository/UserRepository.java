package com.hendisantika.springbootoauth2.repository;

import com.hendisantika.springbootoauth2.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-oauth2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 28/04/20
 * Time: 07.52
 */
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
