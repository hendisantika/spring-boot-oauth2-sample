package com.hendisantika.springbootoauth2.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-oauth2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 28/04/20
 * Time: 07.55
 */
@RestController
public class DemoController {

    @GetMapping("${app.endpoint.index}")
    public String index() {
        return "Hello! " + LocalDateTime.now();
    }

    @GetMapping("${app.endpoint.testEndpoint}")
    public String testAuth() {
        return "Auth Success! " + LocalDateTime.now();
    }
}
