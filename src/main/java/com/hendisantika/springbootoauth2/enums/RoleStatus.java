package com.hendisantika.springbootoauth2.enums;

import lombok.Getter;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-oauth2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 28/04/20
 * Time: 07.36
 */
public enum RoleStatus {
    ACTIVE("Active", "A"),
    INACTIVE("Inactive", "I"),
    DELETED("Deleted", "D");

    @Getter
    private final String label;

    @Getter
    private final String value;

    RoleStatus(String label, String value) {
        this.label = label;
        this.value = value;
    }

    public static RoleStatus getEnum(String value) {
        for (RoleStatus item : RoleStatus.values()) {
            if (item.getValue().equalsIgnoreCase(value)) {
                return item;
            }
        }
        return null;
    }
}
