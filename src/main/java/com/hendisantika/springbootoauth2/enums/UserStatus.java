package com.hendisantika.springbootoauth2.enums;

import lombok.Getter;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-oauth2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 28/04/20
 * Time: 07.35
 */
public enum UserStatus {
    ACTIVE("Active", "A"),
    INACTIVE("Inactive", "I"),
    DELETED("Deleted", "D"),
    CREATED("Created", "C"),
    PENDING_ACTIVATION("Pending Activation", "PEA"),
    TEMP_LOCKED_BAD_CREDENTIALS("Temp Locked Bad Credentials", "TELBC");

    @Getter
    private final String label;

    @Getter
    private final String value;

    UserStatus(String label, String value) {
        this.label = label;
        this.value = value;
    }

    public static UserStatus getEnum(String value) {
        for (UserStatus item : UserStatus.values()) {
            if (item.getValue().equalsIgnoreCase(value)) {
                return item;
            }
        }
        return null;
    }
}
