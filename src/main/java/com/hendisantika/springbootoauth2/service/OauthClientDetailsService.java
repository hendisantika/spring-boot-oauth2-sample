package com.hendisantika.springbootoauth2.service;

import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;

import javax.sql.DataSource;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-oauth2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 28/04/20
 * Time: 07.53
 */
public class OauthClientDetailsService extends JdbcClientDetailsService {
    public OauthClientDetailsService(DataSource dataSource) {
        super(dataSource);
        this.setSelectClientDetailsSql("select client_id, client_secret, resource_ids, scope, authorized_grant_types," +
                " web_server_redirect_uri, authorities, access_token_validity, refresh_token_validity, " +
                "additional_information, autoapprove from oauth_demo.oauth_client_details where client_id = ?");
    }
}
