package com.hendisantika.springbootoauth2.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-oauth2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 28/04/20
 * Time: 07.40
 */
@Data
@Configuration
@ConfigurationProperties("app")
public class ApplicationProperties {

    private Auth auth;
    private Endpoint endpoint;

    @Data
    public static class Auth {

        private String resourceId;
        private String kfName;
        private String ksPass;

        private Integer defaultAccessTokenTimeout;
        private Integer defaultRefreshTokenTimeout;
        private Integer failedLoginAttemptAccountLockTimeout;
        private Integer maxFailedLoginAttemptsForAccountLock;

    }

    @Data
    public static class Endpoint {

        private String index;
        private String testEndpoint;

    }
}
